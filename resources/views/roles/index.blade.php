@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @include('flash-message')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ _i('Nuevo Rol') }}</h3>
                    </div>
                    <div class="panel-body">

                        <div>

                            <div class="col-md-12" style="margin-bottom:15px;">
                                <a href="{{Route('roles.create')}}" class="btn btn-xs btn-default"><i class="fas fa-user"></i> {{ _i('Nuevo Rol') }}</a>
                                <a href="{{Route('permissions.create')}}" class="btn btn-xs btn-default"><i class="fas fa-lock-open"></i> {{ _i('Nuevo Permiso') }}</a>
                            </div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                @foreach ($roles as $index => $role)

                                        <td>
                                            <li role="{{ $role->name }}" @if (!$index) class="active" @endif><a href="#{{ $role->name }}" aria-controls="{{ $role->name }}" role="tab" data-toggle="tab">{{ $role->name }}</a></li>
                                        </td>

                                @endforeach

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">



                            @foreach ($roles as $index => $role)

                                    <div role="{{ $role->name }}" class="tab-pane @if (!$index) active @endif" id="{{ $role->name }}">

                                        <div style="margin-top:20px;"><p>El rol <b>{{$role->name}}</b> tiene los siguientes permisos:</p></div>


                                    @foreach ($role->permissions as $permission)


                                                {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                                                {{Form::label($permission->name, ucfirst($permission->name)) }}
                                            @if(auth()->user()->can('delete permission') && $role->id == 1)

                                            {!! Form::submit('X', ['class' => 'btn btn-xs btn-danger']) !!}
                                            @endcan

                                            {!! Form::close() !!}


                                        @endforeach

                                    <br>

                                        @can('edit role')
                                            <a href="{{Route('roles.edit', $role->id)}}" class="btn btn-default"><i class="far fa-edit"></i> {{ _i('Editar Rol') }}</a>
                                        @endcan
                                        @if(auth()->user()->can('delete role') && $role->id != 1)

                                            {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                                            {!! Form::submit(_i('Borrar Role'), ['class' => 'btn btn-xs btn-danger']) !!}
                                            {!! Form::close() !!}

                                        @endcan


                                    </div>

                                @endforeach


                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
