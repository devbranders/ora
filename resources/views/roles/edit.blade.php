@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @include('flash-message')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Rol {{$role->name}}</h3>
                    </div>
                    <div class="panel-body">

                        {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

                        {{Form::label(_i('Nombre del Rol')) }}
                        {{Form::text('name'), $role->name}}<br><br>

                    @foreach ($permissions as $p)


                            {{Form::checkbox('permissions[]',  $p->id, $role->permissions ) }}
                            {{Form::label($p->name, ucfirst($p->name)) }}<br>



                        @endforeach

                        <br>

                        @can('edit role')
                            <a href="{{Route('roles.index')}}" class="btn btn-default"><i class="fas fa-chevron-circle-left"></i></a>
                        @endcan

                        @if(auth()->user()->can('update role') || auth()->user()->role('root'))
                        {{ Form::submit(_i('Guardar'), array('class' => 'btn btn-primary')) }}
                        @endif



                        {{ Form::close() }}
                    </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
