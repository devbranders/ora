@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @include('flash-message')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{_i('Nuevo Rol')}}</h3>
                    </div>
                    <div class="panel-body">

                        {{ Form::open(['url' => Route('roles.store'), 'method' => 'post']) }}

                        {{Form::label(_i('Nombre')) }}
                        {{Form::text('name')}}<br><br>

                        @foreach ($permissions as $p)

                            {{Form::checkbox('permissions[]',  $p->id ) }}
                            {{Form::label($p->name, ucfirst($p->name)) }}<br>

                        @endforeach

                        <br>

                        @can('edit role')
                            <a href="{{Route('roles.index')}}" class="btn btn-default"><i class="fas fa-chevron-circle-left"></i></a>
                        @endcan

                        @if(auth()->user()->can('create role') || auth()->user()->role('root'))
                            {{ Form::submit(_i('Crear'), array('class' => 'btn btn-success')) }}
                        @endif


                        {{ Form::close() }}
                    </div>


                </div>
            </div>

        </div>
    </div>
    </div>
@endsection
