@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @include('flash-message')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Nuevo Permiso</h3>
                    </div>
                    <div class="panel-body">

                        {{ Form::open(['url' => Route('permissions.store'), 'method' => 'post']) }}

                        {{Form::label('Permission Name') }}
                        {{Form::text('name')}}<br><br>

                        @can('edit role')
                            <a href="{{Route('roles.index')}}" class="btn btn-default"><i class="fas fa-chevron-circle-left"></i></a>
                        @endcan

                        @can('add permission')
                            {{ Form::submit('Create', array('class' => 'btn btn-success')) }}
                        @endcan


                        {{ Form::close() }}
                    </div>


                </div>
            </div>

        </div>
    </div>
    </div>
@endsection
