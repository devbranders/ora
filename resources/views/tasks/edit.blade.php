@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @include('flash-message')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ $task->name }}</h3>
                    </div>
                    <div class="panel-body">

                        {{ Form::model($task, array('route' => array('tasks.update', $task->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}

                        {{ Form::hidden('project_id', $task->project->id) }}

                        <div class="form-group">
                            <label for="task_type" class="col-sm-2 col-md-3  control-label">{{ _i('Tipo') }}</label>
                            <div class="col-sm-10 col-md-8">
                                <select id="task_type" name="task_type" class="form-control">
                                    <option selected="selected">-- {{ _i('Seleccione Tipo') }} --</option>
                                    @foreach($task_types as $task_type)
                                        @if($task_type->id == $task->task_type_id)
                                        <option value="{{ $task_type->id }}" selected="selected"> {{ $task_type->title }}</option>
                                        @else
                                            <option value="{{ $task_type->id }}"> {{ $task_type->title }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputStatus" class="col-sm-2 col-md-3  control-label">{{ _i('Estado') }}</label>
                            <div class="col-sm-10 col-md-8">
                                <select id="inputStatus" name="task_status" class="form-control">
                                    <option selected="selected">-- {{ _i('Seleccione Estado') }} --</option>
                                    @foreach($task_statuses as $task_status)
                                        @if($task_status->id == $task->task_status_id)
                                        <option value="{{ $task_status->id }}" selected="selected"> {{ $task_status->title }}</option>
                                        @else
                                            <option value="{{ $task_status->id }}"> {{ $task_status->title }}</option>
                                        @endif

                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputName" class="col-md-3 col-sm-2 control-label">{{ _i('Nombre') }}</label>
                            <div class="col-sm-10 col-md-8">
                                <input type="text" name="name" value="{{$task->name}}" class="form-control" id="inputName" placeholder="{{ _i('Nombre') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputDesc" class="col-md-3 col-sm-2 control-label">{{ _i('Descripción') }}</label>
                            <div class="col-sm-10  col-md-8">
                                <textarea name="description" class="form-control" id="inputDesc" placeholder="{{ _i('Descripción de la tarea') }}">{{$task->description}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputStart" class="col-md-3 col-sm-2 control-label">{{ _i('Fecha de inicio') }}</label>
                            <div class="col-sm-10 col-md-8">
                                <input type="text" name="start_at" value="{{$task->start_at}}" class="form-control" id="inputStart" placeholder="{{ _i('Nombre') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEnd" class="col-md-3 col-sm-2 control-label">{{ _i('Fecha de finalización') }}</label>
                            <div class="col-sm-10 col-md-8">
                                <input type="text" name="end_at" value="{{$task->end_at}}" class="form-control" id="inputEnd" placeholder="{{ _i('Nombre') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="task_user" class="col-sm-2 col-md-3 control-label">{{ _i('Asignado a') }}</label>
                            <div class="col-sm-10 col-md-8">
                                <select id="task_user" name="task_user" class="form-control">
                                    <option selected="selected">-- {{ _i('Seleccione Usuario') }} --</option>
                                    @foreach($users as $user)
                                        @if($user->id == $task->user_id)
                                        <option value="{{ $user->id }}" selected="selected"> {{ $user->name }}</option>
                                        @else
                                            <option value="{{ $user->id }}"> {{ $user->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        @if(auth()->user()->can('update task') || auth()->user()->role('root'))
                            {{ Form::submit(_i('Guardar'), array('class' => 'btn btn-success')) }}
                        @endif

                    </div>


                    {{ Form::close() }}

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>


                </div>
            </div>

        </div>
    </div>
    </div>
@endsection
