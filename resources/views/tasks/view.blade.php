@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            @include('flash-message')

            <div class="col-md-6">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fas fa-info-circle"></i> {{ $task->name }} <a href="{{Route('tasks.edit', $task->id)}}"><i class="far fa-edit pull-right"></i></a></h3>

                    </div>
                    <div class="panel-body">

                        <table class="table">
                            <tbody>

                            <tr>
                                <td style="width:180px;" class="bg-info">{{ _i('Proyecto') }}</td>
                                <td>{{ $task->project->name }}</td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Fecha de creación') }}</td>
                                <td>{{ Carbon\Carbon::parse($task->created_at)->format('d-m-Y H:i:s') }}</td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Tipo') }}</td>
                                <td>
                                <span class="label" style="background-color:{{$task->task_type->color}};">{{$task->task_type->title}}</span>
                                </td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Estado') }}</td>
                                <td><span class="label" style="background-color:{{$task->task_status->color}};">{{$task->task_status->title}}</span></td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Descripción') }}</td>
                                <td>{{$task->description}}</td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Fecha de inicio') }}</td>
                                <td>{{ Carbon\Carbon::parse($task->start_at)->format('d-m-Y') }}</td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Fecha de finalización') }}</td>
                                <td>{{ Carbon\Carbon::parse($task->end_at)->format('d-m-Y') }}</td>
                            </tr>

                            </tbody>
                        </table>

                    </div>


                </div>
            </div>


            <div class="col-md-6">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fas fa-stopwatch"></i> {{ _i('Tiempo por empleado') }} <a style="cursor:pointer;" data-toggle="modal" data-target="#modalAddTime"><i class="fas fa-plus-circle pull-right"></i></a></h3>
                    </div>
                    <div class="panel-body">

                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th width="100">{{ _i('Empleado') }}</th>
                                <th width="100">{{ _i('Tiempo') }}</th>
                                <th width="100">{{ _i('Coste') }}</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th><b>Total</b></th>
                                <th>{{ gmdate("H:i:s", $total[0]->spent) }}h</th>
                                <th>{{ round($total[0]->spent * 0.01527,2) }}€</th>

                            </tr>
                            </tfoot>
                            <tbody>

                            @foreach($times_employees as $time)
                                <tr>
                                    <td width="80">{{ $time->name }}</td>
                                    <td width="80">{{ gmdate("H:i:s",$time->spent) }}h</td>
                                    <td>{{ round($time->spent * 0.01527, 2) }}€</td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>


                    </div>


                </div>
            </div>

            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fas fa-stopwatch"></i> {{ _i('Desglose de tiempo') }} <a style="cursor:pointer;" data-toggle="modal" data-target="#modalAddTime"><i class="fas fa-plus-circle pull-right"></i></a></h3>
                    </div>
                    <div class="panel-body">

                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>{{ _i('Empleado') }}</th>
                                <th>{{ _i('Comentario') }}</th>
                                <th>{{ _i('Inicio') }}</th>
                                <th>{{ _i('Finalización') }}</th>
                                <th>{{ _i('Tiempo empleado') }}</th>
                                <th>{{ _i('Acciones') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($times as $time)
                                <tr>
                                    <td width="80">{{ $time->user->name }}</td>
                                    <td width="200">{{ $time->description }}</td>
                                    <td width="100">{{ Carbon\Carbon::parse($time->start_at)->format('d-m-Y H:i:s') }}</td>
                                    <td width="100">{{ Carbon\Carbon::parse($time->end_at)->format('d-m-Y H:i:s') }}</td>
                                    <td width="80">{{ gmdate("H:i:s",$time->seconds) }}h</td>
                                    <td width="80">
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['times.destroy', $time->id] ]) !!}
                                        {!! Form::submit( _i('Eliminar'), ['class' => 'btn btn-xs btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>


                    </div>


                </div>
            </div>

        </div>
    </div>
    </div>


    <!-- Modal Add Time -->
    <div class="modal fade" id="modalAddTime" tabindex="-1" role="dialog" aria-labelledby="modalAddTime">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ _i('Añadir tiempo') }}</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(['url' => Route('times.store'), 'method' => 'post', 'class' => 'form-horizontal']) }}

                    {{ Form::hidden('time_task', $task->id) }}
                    {{ Form::hidden('project_id', $task->project->id) }}

                    <div class="form-group">
                        <label for="time_user" class="col-sm-2 col-md-3 control-label">{{ _i('Empleado') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <select id="time_user" name="time_user" class="form-control">
                                <option selected="selected">-- {{ _i('Seleccione Empleado') }} --</option>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}"> {{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputStart" class="col-md-3 col-sm-2 control-label">{{ _i('Hora de inicio') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="start_at" class="form-control" id="inputStart" placeholder="{{ _i('d-m-Y 00:00:00') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEnd" class="col-md-3 col-sm-2 control-label">{{ _i('Hora de finalizacion') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="end_at" class="form-control" id="inputEnd" placeholder="{{ _i('d-m-Y 00:00:00') }}">
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ _i('Cerrar') }}</button>
                    @if(auth()->user()->can('create task') || auth()->user()->role('root'))
                        {{ Form::submit(_i('Crear'), array('class' => 'btn btn-success')) }}
                    @endif
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
