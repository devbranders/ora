@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @include('flash-message')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ _i('Proyectos') }}</h3>
                    </div>
                    <div class="panel-body">

                        <div>

                            <div class="col-md-12" style="margin-bottom:15px;">
                                <a href="{{Route('projects.create')}}" class="btn btn-xs btn-default"><i class="fas fa-user"></i> {{ _i('Nuevo Proyecto') }}</a>
                            </div>

                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>{{ _i('Estado') }}</th>
                                    <th>{{ _i('Proyecto') }}</th>
                                    <th>{{ _i('Descripción') }}</th>
                                    <th>{{ _i('Tags') }}</th>
                                    <th>{{ _i('Acciones') }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($projects as $project)

                                    <tr>
                                        <td><span class="label" style="background-color:{{$project->project_status->color}};">{{ $project->project_status->title }}</span></td>
                                        <td>{{ $project->name }}</td>
                                        <td>{{ $project->description }}</td>
                                        <td>

                                            @if(count($project->project_types) > 0)

                                                @foreach($project->project_types as $type)
                                                    <span class="label" style="background-color:{{$type->color}};">{{ $type->title }}</span>
                                                @endforeach

                                            @else
                                                <span class="label label-info">Sin tags asociados.</span>
                                            @endif

                                        </td>
                                        <td>

                                            <a href="{{ route('projects.show', $project->id) }}" class="btn btn-xs btn-warning pull-left" style="margin-right: 3px;">{{ _i('Ver') }}</a>
                                            <a href="{{ route('projects.edit', $project->id) }}" class="btn btn-xs btn-info pull-left" style="margin-right: 3px;">{{ _i('Editar') }}</a>

                                            {!! Form::open(['method' => 'DELETE', 'route' => ['projects.destroy', $project->id] ]) !!}
                                            {!! Form::submit( _i('Borrar'), ['class' => 'btn btn-xs btn-danger']) !!}
                                            {!! Form::close() !!}

                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
