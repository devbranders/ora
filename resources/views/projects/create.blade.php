@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @include('flash-message')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{_i('Nuevo proyecto')}}</h3>
                    </div>
                    <div class="panel-body">

                        {{ Form::open(['url' => Route('projects.store'), 'method' => 'post', 'class' => 'form-horizontal']) }}


                        <div class="form-group">
                            <label for="project_client" class="col-sm-2 control-label">{{ _i('Cliente') }}</label>
                            <div class="col-sm-10">
                                <select id="project_client" name="project_client" class="form-control">
                                    <option selected="selected">-- Seleccione Cliente --</option>
                                    @foreach($clients as $client)
                                        <option value="{{ $client->id }}"> {{ $client->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="project_types" class="col-sm-2 control-label">{{ _i('Tipo') }}</label>
                            <div class="col-sm-10">
                                <select multiple id="project_types" name="project_types[]" class="form-control">
                                    <option selected="selected">-- Seleccione Tipo --</option>
                                    @foreach($project_types as $project_type)
                                        <option value="{{ $project_type->id }}"> {{ $project_type->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputStatus" class="col-sm-2 control-label">{{ _i('Estado') }}</label>
                            <div class="col-sm-10">
                                <select id="inputStatus" name="project_status" class="form-control">
                                    <option selected="selected">-- Seleccione Estado --</option>
                                    @foreach($project_statuses as $project_status)
                                        <option value="{{ $project_status->id }}"> {{ $project_status->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label">{{ _i('Nombre') }}</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" id="inputName" placeholder="{{ _i('Nombre') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputDesc" class="col-sm-2 control-label">{{ _i('Descripción') }}</label>
                            <div class="col-sm-10">
                                <input type="text" name="description" class="form-control" id="inputDesc" placeholder="{{ _i('Descripción del proyecto') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputStart" class="col-sm-2 control-label">{{ _i('Fecha de inicio') }}</label>
                            <div class="col-sm-10">
                                <input type="text" name="start_at" class="form-control" id="inputStart" placeholder="{{ _i('Nombre') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEnd" class="col-sm-2 control-label">{{ _i('Fecha de finalización') }}</label>
                            <div class="col-sm-10">
                                <input type="text" name="end_at" class="form-control" id="inputEnd" placeholder="{{ _i('Nombre') }}">
                            </div>
                        </div>


                        @if(auth()->user()->can('create project') || auth()->user()->role('root'))
                            {{ Form::submit(_i('Crear'), array('class' => 'btn btn-success')) }}
                        @endif


                        {{ Form::close() }}

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>


                </div>
            </div>

        </div>
    </div>
    </div>
@endsection
