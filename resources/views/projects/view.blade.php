@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            @include('flash-message')

            <div class="col-md-6">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fas fa-info-circle"></i> {{ $project->name }} <a href="{{Route('projects.edit', $project->id)}}"><i class="far fa-edit pull-right"></i></a></h3>

                    </div>
                    <div class="panel-body">

                        <table class="table">
                            <tbody>

                            <tr>
                                <td style="width:180px;" class="bg-info">{{ _i('Cliente') }}</td>
                                <td>{{ $project->client->name }}</td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Fecha de creación') }}</td>
                                <td>{{ Carbon\Carbon::parse($project->created_at)->format('d-m-Y H:i:s') }}</td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Tipo') }}</td>
                                <td>
                                    @foreach($project->project_types as $project_type)
                                    <span class="label" style="background-color:{{$project_type->color}};">{{$project_type->title}}</span>
                                    @endforeach
                                </td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Estado') }}</td>
                                <td><span class="label" style="background-color:{{$project->project_status->color}};">{{$project->project_status->title}}</span></td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Descripción') }}</td>
                                <td>{{$project->description}}</td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Fecha de inicio') }}</td>
                                <td>{{ Carbon\Carbon::parse($project->start_at)->format('d-m-Y') }}</td>
                            </tr>

                            <tr>
                                <td class="bg-info">{{ _i('Fecha de finalización') }}</td>
                                <td>{{ Carbon\Carbon::parse($project->end_at)->format('d-m-Y') }}</td>
                            </tr>

                            </tbody>
                        </table>

                    </div>


                </div>
            </div>


            <div class="col-md-6">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fas fa-stopwatch"></i> {{ _i('Tiempos') }} <a style="cursor:pointer;" data-toggle="modal" data-target="#modalAddTime"><i class="fas fa-plus-circle pull-right"></i></a></h3>
                    </div>
                    <div class="panel-body">

                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th width="100">{{ _i('Empleado') }}</th>
                                <th width="100">{{ _i('Tiempo') }}</th>
                                <th width="100">{{ _i('Coste') }}</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th><b>Total</b></th>
                                <th>{{ gmdate("H:i:s", $total[0]->spent) }}h</th>
                                <th>{{ round($total[0]->spent * 0.01527,2) }}€</th>
                            </tr>
                            </tfoot>
                            <tbody>

                            @foreach($times as $time)
                                <tr>
                                    <td width="80">{{ $time->name }}</td>
                                    <td width="80">{{ gmdate("H:i:s",$time->spent) }}h</td>
                                    <td>{{ round($time->spent * 0.01527, 2) }}€</td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>


                    </div>


                </div>
            </div>


            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fas fa-tasks"></i> {{ _i('Tareas') }} <a style="cursor:pointer;" data-toggle="modal" data-target="#modalCreateTask"><i class="fas fa-plus-circle pull-right"></i></a></h3>
                    </div>
                    <div class="panel-body">


                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>{{ _i('Creada') }}</th>
                                <th>{{ _i('Tipo') }}</th>
                                <th>{{ _i('Estado') }}</th>
                                <th>{{ _i('Tarea') }}</th>
                                <th>{{ _i('Tiempo') }}</th>
                                <th>{{ _i('Asignada') }}</th>
                                <th>{{ _i('Acciones') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($tasks as $task)
                                <tr>
                                    <td width="80">{{ Carbon\Carbon::parse($project->created_at)->format('d/m/Y') }}</td>
                                    <td width="80"><span class="label" style="background-color:{{ $task->task_type->color }};">{{ $task->task_type->title }}</span></td>
                                    <td width="80"><span class="label" style="background-color:{{ $task->task_status->color }};">{{ $task->task_status->title }}</span></td>
                                    <td>{{ $task->name }}</td>
                                    <td>{{  gmdate("H:i:s", $task->times->sum('seconds'))  }} h.</td>
                                    <td width="120">{{ $task->user->name }}</td>
                                    <td width="130">
                                        <a href="{{route('tasks.show',$task->id)}}" class="btn btn-xs btn-info"><i class="fas fa-eye"></i></a>
                                        <a href="{{route('tasks.edit',$task->id)}}" class="btn btn-xs btn-warning"><i class="far fa-edit"></i></a>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['tasks.destroy', $task->id] ]) !!}
                                        {!! Form::submit( _i('Eliminar'), ['class' => 'btn btn-xs btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>


                    </div>


                </div>
            </div>


        </div>
    </div>
    </div>

    <!-- Modal Create Task -->
    <div class="modal fade" id="modalCreateTask" tabindex="-1" role="dialog" aria-labelledby="modalCreateTask">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ _i('Nueva tarea') }}</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(['url' => Route('tasks.store'), 'method' => 'post', 'class' => 'form-horizontal']) }}

                    {{ Form::hidden('project_id', $project->id) }}

                    <div class="form-group">
                        <label for="task_type" class="col-sm-2 col-md-3  control-label">{{ _i('Tipo') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <select id="task_type" name="task_type" class="form-control">
                                <option selected="selected">-- {{ _i('Seleccione Tipo') }} --</option>
                                @foreach($task_types as $task_type)
                                    <option value="{{ $task_type->id }}"> {{ $task_type->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputStatus" class="col-sm-2 col-md-3  control-label">{{ _i('Estado') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <select id="inputStatus" name="task_status" class="form-control">
                                <option selected="selected">-- {{ _i('Seleccione Estado') }} --</option>
                                @foreach($task_statuses as $task_status)
                                    <option value="{{ $task_status->id }}"> {{ $task_status->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputName" class="col-md-3 col-sm-2 control-label">{{ _i('Nombre') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="name" class="form-control" id="inputName" placeholder="{{ _i('Nombre') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputDesc" class="col-md-3 col-sm-2 control-label">{{ _i('Descripción') }}</label>
                        <div class="col-sm-10  col-md-8">
                            <textarea name="description" class="form-control" id="inputDesc" placeholder="{{ _i('Descripción de la tarea') }}"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputStart" class="col-md-3 col-sm-2 control-label">{{ _i('Fecha de inicio') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="start_at" class="form-control" id="inputStart" placeholder="{{ _i('Nombre') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEnd" class="col-md-3 col-sm-2 control-label">{{ _i('Fecha de finalización') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="end_at" class="form-control" id="inputEnd" placeholder="{{ _i('Nombre') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="task_user" class="col-sm-2 col-md-3 control-label">{{ _i('Asignar') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <select id="task_user" name="task_user" class="form-control">
                                <option selected="selected">-- {{ _i('Seleccione Usuario') }} --</option>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}"> {{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ _i('Cerrar') }}</button>
                    @if(auth()->user()->can('create task') || auth()->user()->role('root'))
                        {{ Form::submit(_i('Crear'), array('class' => 'btn btn-success')) }}
                    @endif
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>


    <!-- Modal Add Time -->
    <div class="modal fade" id="modalAddTime" tabindex="-1" role="dialog" aria-labelledby="modalCreateTask">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ _i('Añadir tiempo') }}</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(['url' => Route('times.store'), 'method' => 'post', 'class' => 'form-horizontal']) }}

                    {{ Form::hidden('project_id', $project->id) }}

                    <div class="form-group">
                        <label for="time_task" class="col-sm-2 col-md-3  control-label">{{ _i('Tipo') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <select id="time_task" name="time_task" class="form-control">
                                <option selected="selected">-- {{ _i('Seleccione Tarea') }} --</option>
                                @foreach($tasks as $task)
                                    <option value="{{ $task->id }}"> {{ $task->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="time_user" class="col-sm-2 col-md-3 control-label">{{ _i('Empleado') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <select id="time_user" name="time_user" class="form-control">
                                <option selected="selected">-- {{ _i('Seleccione Empleado') }} --</option>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}"> {{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputStart" class="col-md-3 col-sm-2 control-label">{{ _i('Hora de inicio') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="start_at" class="form-control" id="inputStart" placeholder="{{ _i('d-m-Y 00:00:00') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEnd" class="col-md-3 col-sm-2 control-label">{{ _i('Hora de finalizacion') }}</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="end_at" class="form-control" id="inputEnd" placeholder="{{ _i('d-m-Y 00:00:00') }}">
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ _i('Cerrar') }}</button>
                    @if(auth()->user()->can('create task') || auth()->user()->role('root'))
                        {{ Form::submit(_i('Crear'), array('class' => 'btn btn-success')) }}
                    @endif
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
