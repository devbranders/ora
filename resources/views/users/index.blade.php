@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @include('flash-message')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ _i('Usuarios') }}</h3>
                    </div>
                    <div class="panel-body">

                        <div>

                            <div class="col-md-12" style="margin-bottom:15px;">
                                <a href="{{Route('users.create')}}" class="btn btn-xs btn-default"><i class="fas fa-user"></i> {{ _i('Nuevo Usuario') }}</a>
                            </div>

                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>{{ _i('Nombre') }}</th>
                                    <th>{{ _i('Correo Electrónico') }}</th>
                                    <th>{{ _i('Rol') }}</th>
                                    <th>{{ _i('Acciones') }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($users as $user)

                                    <tr>
                                        <th scope="row">{{ $user->id }}</th>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->roles()->pluck('name')->implode(' ') }}</td>
                                        <td>

                                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-xs btn-info pull-left" style="margin-right: 3px;">{{ _i('Editar') }}</a>

                                            {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id] ]) !!}
                                            {!! Form::submit( _i('Borrar'), ['class' => 'btn btn-xs btn-danger']) !!}
                                            {!! Form::close() !!}

                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
