@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @include('flash-message')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{_i('Nuevo usuario')}}</h3>
                    </div>
                    <div class="panel-body">

                        {{ Form::open(['url' => Route('users.store'), 'method' => 'post', 'class' => 'form-horizontal']) }}


                        <div class="form-group">
                            <label for="inputRol" class="col-sm-2 control-label">{{ _i('Rol') }}</label>
                            <div class="col-sm-10">
                                <select id="inputRol" name="role" class="form-control">
                                    <option selected="selected">-- Seleccione Rol --</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->name }}"> {{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label">{{ _i('Nombre') }}</label>
                            <div class="col-sm-10">
                                <input type="name" name="name" class="form-control" id="inputName" placeholder="{{ _i('Nombre') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-sm-2 control-label">{{ _i('Correo electrónico') }}</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" class="form-control" id="inputEmail">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label">{{ _i('Clave') }}</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" class="form-control" id="inputPassword" placeholder="{{ _i('Clave') }}">
                            </div>
                        </div>

                        @if(auth()->user()->can('create user') || auth()->user()->role('root'))
                            {{ Form::submit(_i('Crear'), array('class' => 'btn btn-success')) }}
                        @endif


                        {{ Form::close() }}
                    </div>


                </div>
            </div>

        </div>
    </div>
    </div>
@endsection
