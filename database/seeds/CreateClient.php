<?php

use Illuminate\Database\Seeder;

class CreateClient extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Create user
        DB::table('clients')->insert([
            'name'                  => 'LeBranders SL',
            'comercial_name'        => 'LeBranders',
            'nif'                   => 'B11835253',
            'address'               => 'Avda. Juan Melgarejo, 1, Local 57',
            'city'                  => 'El Puerto de Santa María',
            'state'                 => 'Cádiz',
            'email'                 => 'hosting@lebranders.com',
            'phone'                 => '956854434',
        ]);

    }
}
