<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');


        // Permission permissions
        Permission::create(['name' => 'add permission']);
        Permission::create(['name' => 'edit permission']);
        Permission::create(['name' => 'update permission']);
        Permission::create(['name' => 'delete permission']);

        // Role permissions
        Permission::create(['name' => 'add role']);
        Permission::create(['name' => 'edit role']);
        Permission::create(['name' => 'update role']);
        Permission::create(['name' => 'delete role']);

        // create user permissions
        Permission::create(['name' => 'add user']);
        Permission::create(['name' => 'edit user']);
        Permission::create(['name' => 'update user']);
        Permission::create(['name' => 'delete user']);
        Permission::create(['name' => 'view users']);

        // create project permissions
        Permission::create(['name' => 'add project']);
        Permission::create(['name' => 'edit project']);
        Permission::create(['name' => 'update project']);
        Permission::create(['name' => 'delete project']);
        Permission::create(['name' => 'view projects']);




        // create roles and assign existing permissions
        $role = Role::create(['name' => 'root']);
        $role->givePermissionTo('add role');
        $role->givePermissionTo('edit role');
        $role->givePermissionTo('update role');
        $role->givePermissionTo('delete role');

        $role->givePermissionTo('add permission');
        $role->givePermissionTo('edit permission');
        $role->givePermissionTo('update permission');
        $role->givePermissionTo('delete permission');

        $role->givePermissionTo('add user');
        $role->givePermissionTo('edit user');
        $role->givePermissionTo('update user');
        $role->givePermissionTo('delete user');
        $role->givePermissionTo('view users');

        $role->givePermissionTo('add project');
        $role->givePermissionTo('edit project');
        $role->givePermissionTo('update project');
        $role->givePermissionTo('delete project');
        $role->givePermissionTo('view projects');

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('add user');
        $role->givePermissionTo('edit user');
        $role->givePermissionTo('update user');
        $role->givePermissionTo('delete user');
        $role->givePermissionTo('view users');

        $role->givePermissionTo('add project');
        $role->givePermissionTo('edit project');
        $role->givePermissionTo('update project');
        $role->givePermissionTo('delete project');
        $role->givePermissionTo('view projects');


    }
}
