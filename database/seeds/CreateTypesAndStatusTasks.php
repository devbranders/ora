<?php

use Illuminate\Database\Seeder;

class CreateTypesAndStatusTasks extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        // Create user
        DB::table('task_statuses')->insert([
            'title'      => 'Abierto',
            'color'      => '#000000',
        ]);
        // Create user
        DB::table('task_statuses')->insert([
            'title'      => 'En Proceso',
            'color'      => '#66c2a5',
        ]);
        // Create user
        DB::table('task_statuses')->insert([
            'title'      => 'Pendiente Feedback',
            'color'      => '#FBBD5F',
        ]);
        // Create user
        DB::table('task_statuses')->insert([
            'title'      => 'Completado',
            'color'      => '#ff6f69',
        ]);



        // Create user
        DB::table('task_types')->insert([
            'title'      => 'Diseño',
            'color'      => '#a86487',
        ]);
        // Create user
        DB::table('task_types')->insert([
            'title'      => 'Desarrollo',
            'color'      => '#8da0cb',
        ]);

    }
}
