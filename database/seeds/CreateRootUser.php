<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class CreateRootUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {

        // Create user
        DB::table('users')->insert([
            'name'      => 'Fernando',
            'email'     => 'fernando@lebranders.com',
            'password'  => bcrypt('123456'),
        ]);

        // Assign role to user
        DB::table('model_has_roles')->insert([
            'role_id' => 1,
            'model_id' => 1,
            'model_type' => 'App\User'
        ]);


    }
}
