<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(CreateRootUser::class);
        $this->call(CreateTypesAndTags::class);
        $this->call(CreateClient::class);
        $this->call(CreateTypesAndStatusTasks::class);
    }
}
