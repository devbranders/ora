<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskHasTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_has_types', function (Blueprint $table) {

            $table->integer('task_id')->unsigned();
            $table->integer('task_type_id')->unsigned();

            // Foreign key
            $table->foreign('task_id')->references('id')->on('tasks');
            $table->foreign('task_type_id')->references('id')->on('task_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_has_types');
    }
}
