<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');


Route::get('/lang/{locale?}', [
    'as'=>'lang',
    'uses'=>'HomeController@changeLang'
]);

Route::group(['middleware' => ['role:root']], function () {


    // Role Route
    Route::resource('roles', 'RoleController');

    // Role Route
    Route::resource('permissions', 'PermissionController');


});


// Users Route
Route::resource('users', 'UserController');

// Projects Route
Route::resource('projects', 'ProjectController');

// Tasks Route
Route::resource('tasks', 'TaskController');

// Times Route
Route::resource('times', 'TimeController');