<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Project extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'project_status_id', 'start_at', 'end_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * Get the post that owns the comment.
     */
    public function project_status()
    {
        return $this->belongsTo('App\ProjectStatus');

    }

    public function project_types()
    {
        return $this->belongsToMany('App\ProjectType');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');

    }
}
