<?php

namespace App\Http\Controllers;

use App\Notifications\RoleCreated;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;


class RoleController extends Controller
{

    use Notifiable;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the role dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        // Get all roles
        $roles = Role::all();

        // View roles' index view
        return view('roles.index')->with('roles', $roles);

    }


    /**
     * Update role.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        $role = Role::findOrFail($id);//Get role with the given id

        //Validate name and permissions field
        $this->validate($request, [
            'name'=>'required|max:10|unique:roles,name,'.$id,
            'permissions' =>'required',
        ]);

        $input = $request->except(['permissions']);
        $role->fill($input)->save();

        // Catch permissions
        $permissions = $request['permissions'];


        // First, we revoke all permissions.
        $p_all = Permission::all();
        foreach ($p_all as $p) {
            $role->revokePermissionTo($p); //Remove all permissions associated with role
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
            $role->givePermissionTo($p);  //Assign permission to role
        }

        return redirect()->route('roles.index')
            ->with('success',
                'Role '. $role->name.' updated!');
    }


    /**
     * Update role.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();

        return view('roles.edit', compact('role', 'permissions'));
    }


    /**
     * Create role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $permissions = Permission::all();

        return view('roles.create', compact('permissions'));
    }


    /**
     * Store role.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        //Validate name and permissions field
        $this->validate($request, [
                'name'=>'required|unique:roles|max:10',
                'permissions' =>'required',
            ]
        );

        $name = $request['name'];
        $role = new Role();
        $role->name = $name;

        $permissions = $request['permissions'];

        $role->save();
        //Looping thru selected permissions
        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail();
            //Fetch the newly created role and assign permission
            $role = Role::where('name', '=', $name)->first();
            $role->givePermissionTo($p);
        }

        // Send notification
        $user = Auth::user();
        $user->notify(new RoleCreated($role,$user));


        return redirect()->route('roles.index')
            ->with('success',
                'Role '. $role->name.' added!');
    }


    /**
     * Destroy role.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect()->route('roles.index')
            ->with('success',
                'Role '.$role->name.' deleted!');

    }

}
