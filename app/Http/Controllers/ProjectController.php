<?php

namespace App\Http\Controllers;

use App\Task;
use App\Time;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\DatabaseManager;
use Spatie\Permission\Models\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use App\Notifications\UserCreated;
use App\User;
use App\Client;
use App\Project;
use App\ProjectTypes;
use App\ProjectStatus;
use App\ProjectType;
use App\TaskStatus;
use App\TaskType;
use Carbon\Carbon;


class ProjectController extends Controller
{

    use Notifiable;



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the project dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Get all roles
        $projects = Project::all();

        // View roles' index view
        return view('projects.index')->with('projects', $projects);

    }

    /**
     * Show the project details.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // Get project details
        $project = Project::findOrFail($id);
        $task_statuses = TaskStatus::all();
        $task_types = TaskType::all();
        $users = User::all();
        $tasks = Task::where('project_id', $id)->get();
        $times = DB::table('times')
            ->leftjoin('tasks', 'times.task_id', '=', 'tasks.id')
            ->leftjoin('projects', 'tasks.project_id', '=', 'projects.id')
            ->leftjoin('users', 'times.user_id', '=', 'users.id')
            ->select('users.name as name', DB::raw('SUM(times.seconds) as spent'))
            ->where('project_id', $id)
            ->groupBy('users.name')
            ->get();

        $total = DB::table('times')
            ->leftjoin('tasks', 'times.task_id', '=', 'tasks.id')
            ->leftjoin('projects', 'tasks.project_id', '=', 'projects.id')
            ->leftjoin('users', 'times.user_id', '=', 'users.id')
            ->select(DB::raw('SUM(times.seconds) as spent') )
            ->where('project_id', $id)
            ->get();


        // View roles' index view
        return view('projects.view', compact('project','task_statuses','task_types','users','tasks', 'times', 'total'));


    }


    /**
     * Create project.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $clients = Client::all();
        $project_statuses = ProjectStatus::all();
        $project_types = ProjectType::all();

        return view('projects.create', compact('project_statuses', 'project_types','clients'));
    }


    /**
     * Store project.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        //Validate name and permissions field
        $this->validate($request, [
                'project_status'=>'required',
                'name'=>'required',
                'start_at'=>'required'
            ]
        );

        // Begin transaction
        DB::beginTransaction();

        // Create project
        $project = new Project();
        $project->name              = $request['name'];
        $project->description       = $request['description'];
        $project->project_status_id = $request['project_status'];
        $project->client_id         = $request['project_client'];
        $project->start_at          = Carbon::createFromFormat('d-m-Y', $request['start_at'])->toDateString();
        $project->end_at            = Carbon::createFromFormat('d-m-Y', $request['end_at'])->toDateString();
        $create = $project->save();
        if(!$create){ DB::rollBack(); }


        // Foreach types
        $project_types = $request['project_types'];
        foreach ($project_types as $project_type) {
            $project_has_types = new ProjectTypes();
            $project_has_types->timestamps = false;
            $project_has_types->project_id      = $project->id;
            $project_has_types->project_type_id = $project_type;
            $create = $project_has_types->save();
            if(!$create){ DB::rollBack(); }
        }

        // DB Commit
        DB::commit();

        return redirect()->route('projects.index')
            ->with('success',
                'Project '. $project->name.' added!');
    }


    /**
     * Destroy project.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::findOrFail($id);
        $project->delete();

        return redirect()->route('projects.index')
            ->with('success',
                'Project '.$project->name.' deleted!');

    }

    /**
     * Update role.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        $project = Project::findOrFail($id);//Get role with the given id

        //Validate name and permissions field
        $this->validate($request, [
                'project_status'=>'required',
                'name'=>'required',
                'start_at'=>'required'
            ]
        );

        // Begin transaction
        DB::beginTransaction();

        // Save
        $project->name              = $request['name'];
        $project->description       = $request['description'];
        $project->project_status_id = $request['project_status'];
        $project->client_id         = $request['project_client'];
        $project->start_at          = Carbon::createFromFormat('d-m-Y', $request['start_at'])->toDateString();
        $project->end_at            = Carbon::createFromFormat('d-m-Y', $request['end_at'])->toDateString();
        $save = $project->save();

        if(!$save){ DB::rollBack(); }

        // Remove types relations
        $delete = DB::table('project_project_type')->where('project_id', $id)->delete();
        if(!$delete){ DB::rollBack(); }


        // Foreach types
        $project_types = $request['project_types'];
        foreach ($project_types as $project_type) {
            $project_has_types = new ProjectTypes();
            $project_has_types->timestamps = false;
            $project_has_types->project_id      = $project->id;
            $project_has_types->project_type_id = $project_type;
            $save = $project_has_types->save();
            if(!$save){ DB::rollBack(); }
        }

        // DB Commit
        DB::commit();

        return redirect()->route('projects.index')
            ->with('success',
                'Project: '. $project->name.' updated!');
    }


    /**
     * Update role.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $project = Project::findOrFail($id);
        $project_statuses = ProjectStatus::all();
        $project_types = ProjectType::all();
        $ppt = ProjectTypes::all()->where('project_id', $id)->pluck('project_type_id')->toArray();
        $clients = Client::all();


        return view('projects.edit', compact('project', 'project_statuses', 'project_types', 'ppt', 'clients'));
    }


}
