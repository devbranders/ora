<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Notifications\Notifiable;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Notifications\UserCreated;

class UserController extends Controller
{

    use Notifiable;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the user dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Get all roles
        $users = User::all();

        // View roles' index view
        return view('users.index')->with('users', $users);

    }

    /**
     * Show create user view.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $roles = Role::all();

        return view('users.create', compact('roles'));
    }


    /**
     * Store the user on database.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        //Validate name and permissions field
        $this->validate($request, [
                'role'      => 'required',
                'name'      => 'required',
                'email'     => 'required|unique:users,email',
                'password'  => 'required'
            ]
        );

        // Create user
        $user = new User();
        $user->name     = $request['name'];
        $user->email    = $request['email'];
        $user->password = Hash::make($request['password']);
        $user->save();

        // Assign role
        $user->assignRole($request['role']);

        // Send notification
        $user->notify(new UserCreated($user));

        // Redirect
        return redirect()->route('users.index')
            ->with('success',
                'User '. $user->name.' added!');
    }

    /**
     * Edit user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $user = User::findOrFail($id);
        $roles = Role::all();

        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * Update user.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        // Get user with the given id
        $user = User::findOrFail($id);

        // Validate name and permissions field
        $this->validate($request, [
                'role'      => 'required',
                'name'      => 'required',
                'email'     => 'required'
            ]
        );

        // Check password
        if($request['password'] != ''){

            $request['password'] = Hash::make($request['password']);
            $input = $request->except(['role']);

        }else{

            $input = $request->except(['role', 'password']);

        }

        // Get inputs & udpdate

        $user->fill($input)->save();

        // Sync roles
        $user->syncRoles($request['role']);

        return redirect()->route('users.index')
            ->with('success',
                'User '. $user->name.' updated!');
    }


    /**
     * Destroy user.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users.index')
            ->with('success',
                'User '.$user->name.' deleted!');

    }

}
