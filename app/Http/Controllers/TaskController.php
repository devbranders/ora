<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Notifications\TaskAssigned;
use App\Task;
use App\Time;
use App\User;
use App\TaskStatus;
use App\TaskType;
use App\Notifications\UserCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

class TaskController extends Controller
{

    use Notifiable;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show create user view.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        // Get task data
        $task = Task::findOrfail($id);
        $task_statuses = TaskStatus::all();
        $task_types = TaskType::all();
        $users = User::all();
        $times = Time::where('task_id',$id)->get();
        $times_employees = DB::table('times')
            ->leftjoin('tasks', 'times.task_id', '=', 'tasks.id')
            ->leftjoin('projects', 'tasks.project_id', '=', 'projects.id')
            ->leftjoin('users', 'times.user_id', '=', 'users.id')
            ->select('users.name as name', DB::raw('SUM(times.seconds) as spent'))
            ->where('task_id', $id)
            ->groupBy('users.name')
            ->get();

        $total = DB::table('times')
            ->leftjoin('tasks', 'times.task_id', '=', 'tasks.id')
            ->leftjoin('projects', 'tasks.project_id', '=', 'projects.id')
            ->leftjoin('users', 'times.user_id', '=', 'users.id')
            ->select(DB::raw('SUM(times.seconds) as spent') )
            ->where('task_id', $id)
            ->get();

        // View roles' index view
        return view('tasks.view', compact('task','task_statuses','task_types','users', 'total', 'times', 'times_employees'));
    }


    /**
     * Store the user on database.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        //Validate name and permissions field
        $this->validate($request, [
                'task_type'      => 'required',
                'task_status'    => 'required',
                'name'              => 'required',
                'start_at'          => 'required',
                'task_user'         => 'required'
            ]
        );

        // Create user
        $task = new Task();
        $task->name             = $request['name'];
        $task->description      = $request['description'];
        $task->task_status_id   = $request['task_status'];
        $task->task_type_id     = $request['task_type'];
        $task->start_at         = Carbon::createFromFormat('d-m-Y', $request['start_at'])->toDateString();
        $task->end_at           = Carbon::createFromFormat('d-m-Y', $request['end_at'])->toDateString();
        $task->project_id       = $request['project_id'];
        $task->user_id          = $request['task_user'];
        $task->save();

        // Send notification
        $user = User::findOrFail($task->user_id);
        $user->notify(new TaskAssigned($task));

        // Redirect
        return redirect()->route('projects.show', $request['project_id'])
            ->with('success',
                'Task '. $task->name.' added!');
    }


    /**
     * Save task.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        // Find task
        $task = Task::findOrFail($id);//Get role with the given id


        //Validate name and permissions field
        $this->validate($request, [
                'task_type'      => 'required',
                'task_status'    => 'required',
                'name'              => 'required',
                'start_at'          => 'required',
                'task_user'         => 'required'
            ]
        );

        // Create user
        $task->name             = $request['name'];
        $task->description      = $request['description'];
        $task->task_status_id   = $request['task_status'];
        $task->task_type_id     = $request['task_type'];
        $task->start_at         = Carbon::createFromFormat('d-m-Y', $request['start_at'])->toDateString();
        $task->end_at           = Carbon::createFromFormat('d-m-Y', $request['end_at'])->toDateString();
        $task->project_id       = $request['project_id'];
        $task->user_id          = $request['task_user'];
        $task->save();


        // Redirect
        return redirect()->route('projects.show', $request['project_id'])
            ->with('success',
                'Task '. $task->name.' added!');
    }

    /**
     * Show create user view.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        // Get task data
        $task = Task::findOrfail($id);
        $task_statuses = TaskStatus::all();
        $task_types = TaskType::all();
        $users = User::all();

        // View roles' index view
        return view('tasks.edit', compact('task','task_statuses','task_types','users'));
    }

    /**
     * Destroy task.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();

        return redirect()->route('projects.show', $task->project->id)
            ->with('success',
                'Task '.$task->title.' deleted!');

    }

}
