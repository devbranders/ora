<?php

namespace App\Http\Controllers;

use App\Task;
use App\Time;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;

class TimeController extends Controller
{

    use Notifiable;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Store the user on database.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        //Validate name and permissions field
        $this->validate($request, [
                'time_task'         => 'required',
                'time_user'         => 'required',
                'start_at'          => 'required',
                'end_at'            => 'required'
            ]
        );

        // Create user
        $time = new Time();
        $time->description      = 'Registrado a través de ORA';
        $time->hourly_fee       = '55.00';
        $time->user_id          = $request['time_user'];
        $time->task_id          = $request['time_task'];
        $time->start_at         = $request['start_at'];
        $time->end_at           = $request['end_at'];
        // Calculate seconds
        $start = Carbon::parse($time->start_at);
        $spent = $start->diffInSeconds(Carbon::parse($time->end_at));

        $time->seconds          = $spent;
        $time->save();


        // Redirect
        return redirect()->route('projects.show', $request['project_id'])
            ->with('success',
                'Time added!');
    }

    /**
     * Destroy task.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $time = Time::findOrFail($id);
        $time->delete();

        return redirect()->route('projects.show', $time->task->project->id)
            ->with('success',
                'Time deleted!');

    }

}
