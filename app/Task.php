<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Task extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'task_status_id', 'start_at', 'end_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];



    /* RELATIONS */
    public function task_status()
    {
        return $this->belongsTo('App\TaskStatus');

    }

    public function task_type()
    {
        return $this->belongsTo('App\TaskType');
    }

    public function user()
    {
        return $this->belongsTo('App\User');

    }

    public function project()
    {
        return $this->belongsTo('App\Project');

    }

    public function times()
    {
        return $this->hasMany('App\Time');

    }
}
