<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php echo $__env->make('flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(_i('Nuevo usuario')); ?></h3>
                    </div>
                    <div class="panel-body">

                        <?php echo e(Form::open(['url' => Route('users.store'), 'method' => 'post', 'class' => 'form-horizontal'])); ?>



                        <div class="form-group">
                            <label for="inputRol" class="col-sm-2 control-label"><?php echo e(_i('Rol')); ?></label>
                            <div class="col-sm-10">
                                <select id="inputRol" name="role" class="form-control">
                                    <option selected="selected">-- Seleccione Rol --</option>
                                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($role->name); ?>"> <?php echo e($role->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label"><?php echo e(_i('Nombre')); ?></label>
                            <div class="col-sm-10">
                                <input type="name" name="name" class="form-control" id="inputName" placeholder="<?php echo e(_i('Nombre')); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-sm-2 control-label"><?php echo e(_i('Correo electrónico')); ?></label>
                            <div class="col-sm-10">
                                <input type="email" name="email" class="form-control" id="inputEmail">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="col-sm-2 control-label"><?php echo e(_i('Clave')); ?></label>
                            <div class="col-sm-10">
                                <input type="password" name="password" class="form-control" id="inputPassword" placeholder="<?php echo e(_i('Clave')); ?>">
                            </div>
                        </div>

                        <?php if(auth()->user()->can('create user') || auth()->user()->role('root')): ?>
                            <?php echo e(Form::submit(_i('Crear'), array('class' => 'btn btn-success'))); ?>

                        <?php endif; ?>


                        <?php echo e(Form::close()); ?>

                    </div>


                </div>
            </div>

        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>