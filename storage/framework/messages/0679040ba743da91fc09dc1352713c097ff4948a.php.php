<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php echo $__env->make('flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Rol <?php echo e($role->name); ?></h3>
                    </div>
                    <div class="panel-body">

                        <?php echo e(Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT'))); ?>


                        <?php echo e(Form::label(_i('Nombre del Rol'))); ?>

                        <?php echo e(Form::text('name'), $role->name); ?><br><br>

                    <?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                            <?php echo e(Form::checkbox('permissions[]',  $p->id, $role->permissions )); ?>

                            <?php echo e(Form::label($p->name, ucfirst($p->name))); ?><br>



                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <br>

                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit role')): ?>
                            <a href="<?php echo e(Route('roles.index')); ?>" class="btn btn-default"><i class="fas fa-chevron-circle-left"></i></a>
                        <?php endif; ?>

                        <?php if(auth()->user()->can('update role') || auth()->user()->role('root')): ?>
                        <?php echo e(Form::submit(_i('Guardar'), array('class' => 'btn btn-primary'))); ?>

                        <?php endif; ?>



                        <?php echo e(Form::close()); ?>

                    </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>