<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php echo $__env->make('flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e($project->name); ?></h3>
                    </div>
                    <div class="panel-body">

                        <?php echo e(Form::model($project, array('route' => array('projects.update', $project->id), 'method' => 'PUT', 'class' => 'form-horizontal'))); ?>




                        <div class="form-group">
                            <label for="project_client" class="col-sm-2 control-label"><?php echo e(_i('Cliente')); ?></label>
                            <div class="col-sm-10">
                                <select id="project_client" name="project_client" class="form-control">
                                    <option>-- Seleccione Cliente --</option>
                                    <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($client->id == $project->client_id): ?>
                                            <option value="<?php echo e($client->id); ?>" selected="selected"> <?php echo e($client->name); ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo e($client->id); ?>"> <?php echo e($client->name); ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="project_types" class="col-sm-2 control-label"><?php echo e(_i('Tipo')); ?></label>
                            <div class="col-sm-10">
                                <select multiple id="project_types" name="project_types[]" class="form-control">
                                    <?php $__currentLoopData = $project_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <?php if(in_array($project_type->id, $ppt)): ?>
                                            <option value="<?php echo e($project_type->id); ?>" selected="selected"> <?php echo e($project_type->title); ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo e($project_type->id); ?>"> <?php echo e($project_type->title); ?></option>
                                        <?php endif; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputStatus" class="col-sm-2 control-label"><?php echo e(_i('Estado')); ?></label>
                            <div class="col-sm-10">
                                <select id="inputStatus" name="project_status" class="form-control">
                                    <option selected="selected">-- Seleccione Estado --</option>
                                    <?php $__currentLoopData = $project_statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project_status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($project_status->id == $project->project_status_id): ?>
                                            <option value="<?php echo e($project_status->id); ?>" selected="selected"> <?php echo e($project_status->title); ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo e($project_status->id); ?>"> <?php echo e($project_status->title); ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label"><?php echo e(_i('Nombre')); ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="name" value="<?php echo e($project->name); ?>" class="form-control" id="inputName" placeholder="<?php echo e(_i('Nombre')); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputDesc" class="col-sm-2 control-label"><?php echo e(_i('Descripción')); ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="description" value="<?php echo e($project->description); ?>" class="form-control" id="inputDesc" placeholder="<?php echo e(_i('Descripción del proyecto')); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputStart" class="col-sm-2 control-label"><?php echo e(_i('Fecha de inicio')); ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="start_at" value="<?php echo e(Carbon\Carbon::parse($project->start_at)->format('d-m-Y')); ?>" class="form-control" id="inputStart" placeholder="<?php echo e(_i('Nombre')); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEnd" class="col-sm-2 control-label"><?php echo e(_i('Fecha de finalización')); ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="end_at" value="<?php echo e(Carbon\Carbon::parse($project->end_at)->format('d-m-Y')); ?>" class="form-control" id="inputEnd" placeholder="<?php echo e(_i('Nombre')); ?>">
                            </div>
                        </div>


                        <?php if(auth()->user()->can('edit project') || auth()->user()->role('root')): ?>
                            <?php echo e(Form::submit(_i('Editar'), array('class' => 'btn btn-success'))); ?>

                        <?php endif; ?>


                        <?php echo e(Form::close()); ?>


                        <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>


                </div>
            </div>

        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>