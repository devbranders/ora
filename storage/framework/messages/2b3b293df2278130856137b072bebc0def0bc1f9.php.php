<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php echo $__env->make('flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(_i('Usuarios')); ?></h3>
                    </div>
                    <div class="panel-body">

                        <div>

                            <div class="col-md-12" style="margin-bottom:15px;">
                                <a href="<?php echo e(Route('users.create')); ?>" class="btn btn-xs btn-default"><i class="fas fa-user"></i> <?php echo e(_i('Nuevo Usuario')); ?></a>
                            </div>

                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th><?php echo e(_i('Nombre')); ?></th>
                                    <th><?php echo e(_i('Correo Electrónico')); ?></th>
                                    <th><?php echo e(_i('Rol')); ?></th>
                                    <th><?php echo e(_i('Acciones')); ?></th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <tr>
                                        <th scope="row"><?php echo e($user->id); ?></th>
                                        <td><?php echo e($user->name); ?></td>
                                        <td><?php echo e($user->email); ?></td>
                                        <td><?php echo e($user->roles()->pluck('name')->implode(' ')); ?></td>
                                        <td>

                                            <a href="<?php echo e(route('users.edit', $user->id)); ?>" class="btn btn-xs btn-info pull-left" style="margin-right: 3px;"><?php echo e(_i('Editar')); ?></a>

                                            <?php echo Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id] ]); ?>

                                            <?php echo Form::submit( _i('Borrar'), ['class' => 'btn btn-xs btn-danger']); ?>

                                            <?php echo Form::close(); ?>


                                        </td>
                                    </tr>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>