<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php echo $__env->make('flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(_i('Nuevo Rol')); ?></h3>
                    </div>
                    <div class="panel-body">

                        <div>

                            <div class="col-md-12" style="margin-bottom:15px;">
                                <a href="<?php echo e(Route('roles.create')); ?>" class="btn btn-xs btn-default"><i class="fas fa-user"></i> <?php echo e(_i('Nuevo Rol')); ?></a>
                                <a href="<?php echo e(Route('permissions.create')); ?>" class="btn btn-xs btn-default"><i class="fas fa-lock-open"></i> <?php echo e(_i('Nuevo Permiso')); ?></a>
                            </div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <td>
                                            <li role="<?php echo e($role->name); ?>" <?php if(!$index): ?> class="active" <?php endif; ?>><a href="#<?php echo e($role->name); ?>" aria-controls="<?php echo e($role->name); ?>" role="tab" data-toggle="tab"><?php echo e($role->name); ?></a></li>
                                        </td>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">



                            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <div role="<?php echo e($role->name); ?>" class="tab-pane <?php if(!$index): ?> active <?php endif; ?>" id="<?php echo e($role->name); ?>">

                                        <div style="margin-top:20px;"><p>El rol <b><?php echo e($role->name); ?></b> tiene los siguientes permisos:</p></div>


                                    <?php $__currentLoopData = $role->permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                                                <?php echo Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]); ?>

                                                <?php echo e(Form::label($permission->name, ucfirst($permission->name))); ?>

                                            <?php if(auth()->user()->can('delete permission') && $role->id == 1): ?>

                                            <?php echo Form::submit('X', ['class' => 'btn btn-xs btn-danger']); ?>

                                            <?php endif; ?>

                                            <?php echo Form::close(); ?>



                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <br>

                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit role')): ?>
                                            <a href="<?php echo e(Route('roles.edit', $role->id)); ?>" class="btn btn-default"><i class="far fa-edit"></i> <?php echo e(_i('Editar Rol')); ?></a>
                                        <?php endif; ?>
                                        <?php if(auth()->user()->can('delete role') && $role->id != 1): ?>

                                            <?php echo Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]); ?>

                                            <?php echo Form::submit(_i('Borrar Role'), ['class' => 'btn btn-xs btn-danger']); ?>

                                            <?php echo Form::close(); ?>


                                        <?php endif; ?>


                                    </div>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>