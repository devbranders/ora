<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">

            <?php echo $__env->make('flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <div class="col-md-6">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fas fa-info-circle"></i> <?php echo e($project->name); ?> <a href="<?php echo e(Route('projects.edit', $project->id)); ?>"><i class="far fa-edit pull-right"></i></a></h3>

                    </div>
                    <div class="panel-body">

                        <table class="table">
                            <tbody>

                            <tr>
                                <td style="width:180px;" class="bg-info"><?php echo e(_i('Cliente')); ?></td>
                                <td><?php echo e($project->client->name); ?></td>
                            </tr>

                            <tr>
                                <td class="bg-info"><?php echo e(_i('Fecha de creación')); ?></td>
                                <td><?php echo e(Carbon\Carbon::parse($project->created_at)->format('d-m-Y H:i:s')); ?></td>
                            </tr>

                            <tr>
                                <td class="bg-info"><?php echo e(_i('Tipo')); ?></td>
                                <td>
                                    <?php $__currentLoopData = $project->project_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <span class="label" style="background-color:<?php echo e($project_type->color); ?>;"><?php echo e($project_type->title); ?></span>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </td>
                            </tr>

                            <tr>
                                <td class="bg-info"><?php echo e(_i('Estado')); ?></td>
                                <td><span class="label" style="background-color:<?php echo e($project->project_status->color); ?>;"><?php echo e($project->project_status->title); ?></span></td>
                            </tr>

                            <tr>
                                <td class="bg-info"><?php echo e(_i('Descripción')); ?></td>
                                <td><?php echo e($project->description); ?></td>
                            </tr>

                            <tr>
                                <td class="bg-info"><?php echo e(_i('Fecha de inicio')); ?></td>
                                <td><?php echo e(Carbon\Carbon::parse($project->start_at)->format('d-m-Y')); ?></td>
                            </tr>

                            <tr>
                                <td class="bg-info"><?php echo e(_i('Fecha de finalización')); ?></td>
                                <td><?php echo e(Carbon\Carbon::parse($project->end_at)->format('d-m-Y')); ?></td>
                            </tr>

                            </tbody>
                        </table>

                    </div>


                </div>
            </div>


            <div class="col-md-6">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fas fa-stopwatch"></i> <?php echo e(_i('Tiempos')); ?> <a style="cursor:pointer;" data-toggle="modal" data-target="#modalAddTime"><i class="fas fa-plus-circle pull-right"></i></a></h3>
                    </div>
                    <div class="panel-body">

                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th><?php echo e(_i('Empleado')); ?></th>
                                <th><?php echo e(_i('Tiempo')); ?></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th><b>Total</b></th>
                                <th><?php echo e(gmdate("H:i:s", $total[0]->spent)); ?>h</th>
                            </tr>
                            </tfoot>
                            <tbody>

                            <?php $__currentLoopData = $times; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $time): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td width="80"><?php echo e($time->name); ?></td>
                                    <td width="80"><?php echo e(gmdate("H:i:s",$time->spent)); ?>h</td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tbody>
                        </table>


                    </div>


                </div>
            </div>


            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fas fa-tasks"></i> <?php echo e(_i('Tareas')); ?> <a style="cursor:pointer;" data-toggle="modal" data-target="#modalCreateTask"><i class="fas fa-plus-circle pull-right"></i></a></h3>
                    </div>
                    <div class="panel-body">


                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th><?php echo e(_i('Creada')); ?></th>
                                <th><?php echo e(_i('Tipo')); ?></th>
                                <th><?php echo e(_i('Estado')); ?></th>
                                <th><?php echo e(_i('Tarea')); ?></th>
                                <th><?php echo e(_i('Tiempo')); ?></th>
                                <th><?php echo e(_i('Asignada')); ?></th>
                                <th><?php echo e(_i('Acciones')); ?></th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td width="80"><?php echo e(Carbon\Carbon::parse($project->created_at)->format('d/m/Y')); ?></td>
                                    <td width="80"><span class="label" style="background-color:<?php echo e($task->task_type->color); ?>;"><?php echo e($task->task_type->title); ?></span></td>
                                    <td width="80"><span class="label" style="background-color:<?php echo e($task->task_status->color); ?>;"><?php echo e($task->task_status->title); ?></span></td>
                                    <td><?php echo e($task->name); ?></td>
                                    <td><?php echo e(gmdate("H:i:s", $task->times->sum('seconds'))); ?> h.</td>
                                    <td width="120"><?php echo e($task->user->name); ?></td>
                                    <td width="130">
                                        <a href="<?php echo e(route('tasks.show',$task->id)); ?>" class="btn btn-xs btn-info"><i class="fas fa-eye"></i></a>
                                        <a href="<?php echo e(route('tasks.edit',$task->id)); ?>" class="btn btn-xs btn-warning"><i class="far fa-edit"></i></a>
                                        <?php echo Form::open(['method' => 'DELETE', 'route' => ['tasks.destroy', $task->id] ]); ?>

                                        <?php echo Form::submit( _i('Eliminar'), ['class' => 'btn btn-xs btn-danger']); ?>

                                        <?php echo Form::close(); ?>

                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tbody>
                        </table>


                    </div>


                </div>
            </div>


        </div>
    </div>
    </div>

    <!-- Modal Create Task -->
    <div class="modal fade" id="modalCreateTask" tabindex="-1" role="dialog" aria-labelledby="modalCreateTask">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo e(_i('Nueva tarea')); ?></h4>
                </div>
                <div class="modal-body">
                    <?php echo e(Form::open(['url' => Route('tasks.store'), 'method' => 'post', 'class' => 'form-horizontal'])); ?>


                    <?php echo e(Form::hidden('project_id', $project->id)); ?>


                    <div class="form-group">
                        <label for="task_type" class="col-sm-2 col-md-3  control-label"><?php echo e(_i('Tipo')); ?></label>
                        <div class="col-sm-10 col-md-8">
                            <select id="task_type" name="task_type" class="form-control">
                                <option selected="selected">-- <?php echo e(_i('Seleccione Tipo')); ?> --</option>
                                <?php $__currentLoopData = $task_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($task_type->id); ?>"> <?php echo e($task_type->title); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputStatus" class="col-sm-2 col-md-3  control-label"><?php echo e(_i('Estado')); ?></label>
                        <div class="col-sm-10 col-md-8">
                            <select id="inputStatus" name="task_status" class="form-control">
                                <option selected="selected">-- <?php echo e(_i('Seleccione Estado')); ?> --</option>
                                <?php $__currentLoopData = $task_statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task_status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($task_status->id); ?>"> <?php echo e($task_status->title); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputName" class="col-md-3 col-sm-2 control-label"><?php echo e(_i('Nombre')); ?></label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="name" class="form-control" id="inputName" placeholder="<?php echo e(_i('Nombre')); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputDesc" class="col-md-3 col-sm-2 control-label"><?php echo e(_i('Descripción')); ?></label>
                        <div class="col-sm-10  col-md-8">
                            <textarea name="description" class="form-control" id="inputDesc" placeholder="<?php echo e(_i('Descripción de la tarea')); ?>"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputStart" class="col-md-3 col-sm-2 control-label"><?php echo e(_i('Fecha de inicio')); ?></label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="start_at" class="form-control" id="inputStart" placeholder="<?php echo e(_i('Nombre')); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEnd" class="col-md-3 col-sm-2 control-label"><?php echo e(_i('Fecha de finalización')); ?></label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="end_at" class="form-control" id="inputEnd" placeholder="<?php echo e(_i('Nombre')); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="task_user" class="col-sm-2 col-md-3 control-label"><?php echo e(_i('Asignar')); ?></label>
                        <div class="col-sm-10 col-md-8">
                            <select id="task_user" name="task_user" class="form-control">
                                <option selected="selected">-- <?php echo e(_i('Seleccione Usuario')); ?> --</option>
                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($user->id); ?>"> <?php echo e($user->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(_i('Cerrar')); ?></button>
                    <?php if(auth()->user()->can('create task') || auth()->user()->role('root')): ?>
                        <?php echo e(Form::submit(_i('Crear'), array('class' => 'btn btn-success'))); ?>

                    <?php endif; ?>
                </div>

                <?php echo e(Form::close()); ?>

            </div>
        </div>
    </div>


    <!-- Modal Add Time -->
    <div class="modal fade" id="modalAddTime" tabindex="-1" role="dialog" aria-labelledby="modalCreateTask">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo e(_i('Añadir tiempo')); ?></h4>
                </div>
                <div class="modal-body">
                    <?php echo e(Form::open(['url' => Route('times.store'), 'method' => 'post', 'class' => 'form-horizontal'])); ?>


                    <?php echo e(Form::hidden('project_id', $project->id)); ?>


                    <div class="form-group">
                        <label for="time_task" class="col-sm-2 col-md-3  control-label"><?php echo e(_i('Tipo')); ?></label>
                        <div class="col-sm-10 col-md-8">
                            <select id="time_task" name="time_task" class="form-control">
                                <option selected="selected">-- <?php echo e(_i('Seleccione Tarea')); ?> --</option>
                                <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($task->id); ?>"> <?php echo e($task->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="time_user" class="col-sm-2 col-md-3 control-label"><?php echo e(_i('Empleado')); ?></label>
                        <div class="col-sm-10 col-md-8">
                            <select id="time_user" name="time_user" class="form-control">
                                <option selected="selected">-- <?php echo e(_i('Seleccione Empleado')); ?> --</option>
                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($user->id); ?>"> <?php echo e($user->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputStart" class="col-md-3 col-sm-2 control-label"><?php echo e(_i('Hora de inicio')); ?></label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="start_at" class="form-control" id="inputStart" placeholder="<?php echo e(_i('d-m-Y 00:00:00')); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEnd" class="col-md-3 col-sm-2 control-label"><?php echo e(_i('Hora de finalizacion')); ?></label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" name="end_at" class="form-control" id="inputEnd" placeholder="<?php echo e(_i('d-m-Y 00:00:00')); ?>">
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(_i('Cerrar')); ?></button>
                    <?php if(auth()->user()->can('create task') || auth()->user()->role('root')): ?>
                        <?php echo e(Form::submit(_i('Crear'), array('class' => 'btn btn-success'))); ?>

                    <?php endif; ?>
                </div>

                <?php echo e(Form::close()); ?>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>