<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the role dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Get all roles
        $roles = Role::all();

        // View roles' index view
        return view('permissions.index')->with('roles', $roles);

    }


    /**
     * Update role.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        $role = Role::findOrFail($id);//Get role with the given id

        //Validate name and permissions field
        $this->validate($request, [
            'name'=>'required|max:20|unique:roles,name,'.$id,
            'permissions' =>'required',
        ]);

        $input = $request->except(['permissions']);
        $role->fill($input)->save();

        // Catch permissions
        $permissions = $request['permissions'];


        // First, we revoke all permissions.
        $p_all = Permission::all();
        foreach ($p_all as $p) {
            $role->revokePermissionTo($p); //Remove all permissions associated with role
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
            $role->givePermissionTo($p);  //Assign permission to role
        }

        return redirect()->route('roles.index')
            ->with('success',
                'Role '. $role->name.' updated!');
    }


    /**
     * Update role.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();

        return view('roles.edit', compact('role', 'permissions'));
    }


    /**
     * Create role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        return view('permissions.create');
    }


    /**
     * Store role.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        //Validate name and permissions field
        $this->validate($request, [
                'name'=>'required|unique:permissions|max:20',
            ]
        );

        $name = $request['name'];
        $permission = new Permission();
        $permission->name = $name;
        $permission->save();

        return redirect()->route('roles.index')
            ->with('success',
                'Permission:'. $permission->name.' added!');
    }


    /**
     * Destroy role.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();

        return redirect()->route('roles.index')
            ->with('success',
                'Permission '.$permission->name.' deleted!');

    }

}
