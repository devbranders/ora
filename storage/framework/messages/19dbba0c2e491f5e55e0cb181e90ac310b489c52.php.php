<?php

namespace App\Http\Controllers;

use App\Notifications\TaskAssigned;
use App\Task;
use App\User;
use App\Notifications\UserCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

class TaskController extends Controller
{

    use Notifiable;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show create user view.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $roles = Role::all();

        return view('users.create', compact('roles'));
    }


    /**
     * Store the user on database.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        //Validate name and permissions field
        $this->validate($request, [
                'task_type'      => 'required',
                'task_status'    => 'required',
                'name'              => 'required',
                'start_at'          => 'required',
                'task_user'         => 'required'
            ]
        );

        // Create user
        $task = new Task();
        $task->name             = $request['name'];
        $task->description      = $request['description'];
        $task->task_status_id   = $request['task_status'];
        $task->task_type_id     = $request['task_type'];
        $task->start_at         = Carbon::createFromFormat('d-m-Y', $request['start_at'])->toDateString();
        $task->end_at           = Carbon::createFromFormat('d-m-Y', $request['end_at'])->toDateString();
        $task->project_id       = $request['project_id'];
        $task->user_id          = $request['task_user'];
        $task->save();

        // Send notification
        $user = User::findOrFail($task->user_id);
        $user->notify(new TaskAssigned($task));

        // Redirect
        return redirect()->route('projects.show', $request['project_id'])
            ->with('success',
                'Task '. $task->name.' added!');
    }

    /**
     * Destroy task.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();

        return redirect()->route('projects.show', $task->project->id)
            ->with('success',
                'Task '.$task->title.' deleted!');

    }

}
