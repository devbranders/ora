<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php echo $__env->make('flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo e(_i('Proyectos')); ?></h3>
                    </div>
                    <div class="panel-body">

                        <div>

                            <div class="col-md-12" style="margin-bottom:15px;">
                                <a href="<?php echo e(Route('projects.create')); ?>" class="btn btn-xs btn-default"><i class="fas fa-user"></i> <?php echo e(_i('Nuevo Proyecto')); ?></a>
                            </div>

                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo e(_i('Estado')); ?></th>
                                    <th><?php echo e(_i('Proyecto')); ?></th>
                                    <th><?php echo e(_i('Descripción')); ?></th>
                                    <th><?php echo e(_i('Tags')); ?></th>
                                    <th><?php echo e(_i('Acciones')); ?></th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <tr>
                                        <td><span class="label" style="background-color:<?php echo e($project->project_status->color); ?>;"><?php echo e($project->project_status->title); ?></span></td>
                                        <td><?php echo e($project->name); ?></td>
                                        <td><?php echo e($project->description); ?></td>
                                        <td>

                                            <?php if(count($project->project_types) > 0): ?>

                                                <?php $__currentLoopData = $project->project_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <span class="label" style="background-color:<?php echo e($type->color); ?>;"><?php echo e($type->title); ?></span>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            <?php else: ?>
                                                <span class="label label-info">Sin tags asociados.</span>
                                            <?php endif; ?>

                                        </td>
                                        <td>

                                            <a href="<?php echo e(route('projects.show', $project->id)); ?>" class="btn btn-xs btn-warning pull-left" style="margin-right: 3px;"><?php echo e(_i('Ver')); ?></a>
                                            <a href="<?php echo e(route('projects.edit', $project->id)); ?>" class="btn btn-xs btn-info pull-left" style="margin-right: 3px;"><?php echo e(_i('Editar')); ?></a>

                                            <?php echo Form::open(['method' => 'DELETE', 'route' => ['projects.destroy', $project->id] ]); ?>

                                            <?php echo Form::submit( _i('Borrar'), ['class' => 'btn btn-xs btn-danger']); ?>

                                            <?php echo Form::close(); ?>


                                        </td>
                                    </tr>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>